/**
 * __   _____  ___         _
 * \ \ / / __|/ __|___  __| |___
 *  \ V /\__ \ (__/ _ \/ _` / -_)
 *   \_/ |___/\___\___/\__,_\___|
 *
 * @Author: Christopher Dowell <cbdowell@gmail.com>
 * @License: MIT
 */
{
    // Breadcrumbs
    // =========================================================================

    "breadcrumbs.enabled": true,


    // Editors
    // =========================================================================

    "editor.minimap.enabled": false,
    "editor.fontFamily": "M+ 1m",
    "editor.fontLigatures": true,
    "editor.fontSize": 12,
    "editor.autoClosingBrackets": "always",
    "editor.autoClosingQuotes": "never",
    "editor.columnSelection": false,
    "editor.cursorBlinking": "phase",
    "editor.cursorSurroundingLines": 4,
    "editor.emptySelectionClipboard": false,
    "editor.find.autoFindInSelection": "multiline",
    "editor.find.seedSearchStringFromSelection": true,
    "editor.formatOnSave": false,
    "editor.formatOnType": false,
    "editor.renderControlCharacters": false,
    "editor.renderLineHighlight": "none",
    "editor.renderWhitespace": "none",
    "editor.suggestSelection": "first",
    "editor.tabCompletion": "on",
    "editor.wordWrap": "off",
    "editor.wordWrapColumn": 120,
    "editor.wrappingIndent": "none",
    "editor.mouseWheelZoom": true,
    "editor.formatOnPaste": false,
    "editor.rulers": [
        80
    ],


    // Emmet
    // =========================================================================

    "emmet.showAbbreviationSuggestions": false,
    "emmet.triggerExpansionOnTab": true,


    // Explorer
    // =========================================================================

    "explorer.autoReveal": false,
    "explorer.enableDragAndDrop": false,
    "explorer.incrementalNaming": "smart",
    "explorer.openEditors.visible": 5,
    "explorer.sortOrder": "default",
    "explorer.compactFolders": false,
    "explorer.confirmDelete": false,
    "explorer.decorations.badges": false,


    // Extensions
    // =========================================================================

    "extensions.autoCheckUpdates": false,
    "extensions.autoUpdate": true,
    "extensions.ignoreRecommendations": true,
    "extensions.showRecommendationsOnlyOnDemand": true,


    // File Utils
    // =========================================================================

    "fileutils.delete.useTrash": true,


    // Files
    // =========================================================================

    "files.autoSave": "off",
    "files.insertFinalNewline": true,
    "files.trimFinalNewlines": false,
    "files.trimTrailingWhitespace": true,
    "files.associations": {
        "**/.i3/config": "i3",
        "**/i3/config": "i3",
        "**/wpg/templates/i3*": "i3",
        "**/templates/vscode*": "jsonc",
        "muttrc": "tmux",
        "**/*_vars/**/*.yml": "ansible",
        "**/*ansible*/**/*.yml": "ansible",
        "**/defaults/**/*": "ansible",
        "**/handler/*.yml": "ansible",
        "**/inventory/*/*": "ansible",
        "**/playbooks/**/*.yml": "ansible",
        "**/roles/**/*.yml": "ansible",
        "**/tasks/**/*.yml": "ansible",
        "**/vars/**/*.yml": "ansible",
        "**/oomox/colors/**": "ini",
        "**/wpg/templates/oomox*": "ini",
        "**/rcrc": "properties",
        "**/tmux.conf": "tmux",
        "**/wpg.conf": "ini",
        "*.json": "jsonc",
        "dir_colors": "dircolors",
        "profile": "shellscript",
        "init-rcrc": "shellscript"
    },


    // Gists
    // =========================================================================

    "gist.defaultPrivate": true,


    // Gist Explorer
    // =========================================================================

    "GithubGistExplorer.explorer.gistSortBy": "Last Updated",
    "GithubGistExplorer.explorer.gistAscending": true,
    "GithubGistExplorer.explorer.subscriptionSortBy": "Last Updated",
    "GithubGistExplorer.explorer.subscriptionAscending": true,
    "GithubGistExplorer.github.token": "f6e91f5abfe6ef1e1b899ab97910c3cba10d975f",
    "GithubGistExplorer.github.username": "cbdowell",


    // Git
    // =========================================================================

    "git.allowForcePush": true,
    "git.alwaysShowStagedChangesResourceGroup": true,
    "git.autofetch": true,
    "git.showProgress": false,
    "git.enableStatusBarSync": false,
    "git.confirmSync": false,


    // =========================================================================
    // Gitlab
    // =========================================================================

    "gitlab.enableExperimentalFeatures": true,
    "gitlab.instanceUrl": "",


    // Syncronize Settings
    // =========================================================================

    "sync.forceUpload": true,
    "sync.autoUpload": true,
    "sync.quietSync": true,
    "sync.gist": "4d221083a73888aa48f36363879a0d31",


    // Formatting Context Menu
    // =========================================================================

    "formatContextMenu.closeAfterSave": true,


    // Favorites
    // =========================================================================

    "favorites.ownExplorer": true,


    // Make
    // =========================================================================

    "makefileCommandRunner.makefileName": "Makefile.main",


    // Npm
    // =========================================================================
    "npm.enableRunFromFolder": true,
    "npm.enableScriptExplorer": true,


    // Rainbow
    // =========================================================================

    "rainbow_csv.enable_context_menu_tail": true,


    // Searching
    // =========================================================================

    "search.collapseResults": "alwaysCollapse",
    "search.followSymlinks": false,
    "search.quickOpen.history.filterSortOrder": "recency",
    "search.quickOpen.includeHistory": false,
    "search.searchEditor.doubleClickBehaviour": "openLocationToSide",
    "search.searchEditor.reusePriorSearchConfiguration": true,
    "search.searchOnType": false,
    "search.seedOnFocus": true,
    "search.smartCase": true,
    "search.sortOrder": "type",
    "search.useGlobalIgnoreFiles": true,
    "search.useIgnoreFiles": true,
    "search.exclude": {
        "**/*.lock": true,
        "**/.git*": true
    },


    // Task Explorer
    // =========================================================================

    "taskExplorer.enableMake": true,
    "taskExplorer.pathToMake": "make",
    "taskExplorer.debug": true,
    "taskExplorer.enableAnsiconForAnt": false,
    "taskExplorer.enableAppPublisher": false,
    "taskExplorer.enableBatch": false,
    "taskExplorer.enableGradle": false,
    "taskExplorer.enableGrunt": false,
    "taskExplorer.enableNpm": false,
    "taskExplorer.enableNsis": false,
    "taskExplorer.enablePerl": false,
    "taskExplorer.enablePowershell": false,
    "taskExplorer.enableRuby": false,
    "taskExplorer.enableSideBar": true,
    "taskExplorer.enableTsc": false,
    "taskExplorer.groupDashed": true,
    "taskExplorer.keepTermOnStop": true,


    // Telementry
    // =========================================================================

    "telemetry.enableCrashReporter": false,
    "telemetry.enableTelemetry": false,


    // Terminals
    // =========================================================================

    "terminal.integrated.fontFamily": "Iosevka Term SS04",
    "terminal.integrated.shell.linux": "/usr/bin/fish",
    "terminal.integrated.shellArgs.linux": [
        "-l"
    ],
    "terminal.explorerKind": "external",


    // Terminals
    // =========================================================================

    "todo-tree.tree.showScanModeButton": false,


    // Updates
    // =========================================================================

    "update.mode": "manual",


    // VS Icons
    // =========================================================================

    "vsicons.dontShowNewVersionMessage": true,

    // Windows
    // =========================================================================

    "window.zoomLevel": 0,
    "window.enableMenuBarMnemonics": false,
    "window.menuBarVisibility": "compact",
    "window.newWindowDimensions": "inherit",


    // Workbench
    // =========================================================================

    "workbench.startupEditor": "newUntitledFile",
    "workbench.quickOpen.closeOnFocusLost": false,
    "workbench.quickOpen.preserveInput": true,
    "workbench.editor.closeEmptyGroups": false,
    "workbench.editor.closeOnFileDelete": true,
    "workbench.settings.editor": "json",
    "workbench.sideBar.location": "left",
    "workbench.statusBar.visible": true,
    "workbench.tips.enabled": false,
    "workbench.tree.indent": 16,
    "workbench.editor.limit.enabled": true,
    "workbench.iconTheme": "vscode-icons",
    "workbench.colorTheme": "Wal Bordered",


    // Specific File Settings
    // =========================================================================

    "[fish]": {
        "editor.defaultFormatter": "lunaryorn.fish-ide"
    },

    "[html]": {
        "editor.defaultFormatter": "vscode.html-language-features"
    },

    "[json]": {
        "editor.defaultFormatter": "vscode.json-language-features"
    },

    "[jsonc]": {
        "editor.defaultFormatter": "vscode.json-language-features"
    },

    "[markdown]": {
        "editor.wordWrap": "off",
        "editor.quickSuggestions": false
    },

    "[shellscript]": {
        "editor.defaultFormatter": "foxundermoon.shell-format"
    },

    "[dockerfile]": {
        "editor.defaultFormatter": "ms-azuretools.vscode-docker"
    },

    "[properties]": {
        "editor.defaultFormatter": "foxundermoon.shell-format"
    }
}
